$(function(){
    var tabsTtiles = $('.tab-title');
    var tabsContent = $('.tab-content');
    

    tabsTtiles.click(function(){
        var activeData = $(this).attr('data-title');
        
        if(activeData != undefined){
            $(this).addClass('active');
            $(tabsTtiles).not(this).removeClass('active');
    
            $('.tab-content[data-content="' + activeData + '"]').addClass('active');
            $('.tab-content').not($('.tab-content[data-content="' + activeData + '"]')).removeClass('active')
        }
    });

    if($(".register-form").length || $(".login-form").length){
        $(".login-form").validate({

            rules: {
                token: {
                    required: true,
                    lettersNumbers: true
                },
                email: {
                    required: true,
                    customemail: true,
                }
            },
            messages: {
                email: {
                    required: 'Por favor ingresa tu email',
                    customemail: 'Por favor ingrea un email valido',
                },
                token: {
                    required: 'Por favor ingresa el token de acceso',
                    lettersNumbers: 'El token debe tener solo letras y numeros'
                }
            },
    
            submitHandler: function () {
            },
            errorPlacement: function(error, element) {
                error.prependTo(element.parent().find('.errors-form'));
            }
        });

        $(".register-form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                },
                lastname: {
                    required: true,
                    minlength: 3,
                },
                email: {
                    required: true,
                    customemail: true,
                },
                mobile: {
                    required: true
                },
                token: {
                    required: true,
                    lettersNumbers: true
                },
            },
            messages: {
                name: {
                    required: 'Por favor ingresa tu nombre',
                    minlength: 'El nombre debe tener minimo 3 caracteres',
                },
                lastname: {
                    required: 'Por favor ingresa tu apellido',
                    minlength: 'El apellido debe tener minimo 3 caracteres',
                },
                email: {
                    required: 'Por favor ingresa tu email',
                    customemail: 'Por favor ingrea un email valido',
                },
                mobile: {
                    required: 'Por favor ingresa tu celular',
                },
                token: {
                    required: 'Por favor ingresa el token de acceso',
                    lettersNumbers: 'El token debe tener solo letras y numeros'
                }
            },
    
            errorPlacement: function(error, element) {
                error.prependTo(element.parent().find('.errors-form'));
            }
        });

        jQuery.validator.addMethod("lettersNumbers", function(value, element) {
            return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
        }, "Input must contain only letters, numbers.");
        jQuery.validator.addMethod("customemail",
            function (value) {
                return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
            }
        );
    }

    $('.add-content-btn').click(function(e){
        e.preventDefault();
        $(this).siblings().fadeToggle('fast')
    });

    $('.open-modal').click(function(e){
        e.preventDefault();
        var iframeSrc = $(this).attr('data-iframe');
        $('.modal-video iframe').attr('src', iframeSrc);
        $('.modal-video').fadeIn('fast');
    });
    $('.close-modal').click(function(e){
        e.preventDefault();
        if($(this).hasClass('meeting-modal')){
            $('.modal-close').fadeIn('fast');
        }else{
            $('.modal-video').fadeOut('fast');
            $('.modal-video iframe').attr('src', '');
        }
    });

    $('.modal-close .main-btn').click(function(e){
        e.preventDefault();
        if($(this).hasClass('leave-meeting')){
            $(this).parents('.modal-close').fadeOut('fast');
            $('.modal-video-meeting').fadeOut('fast');
            window.location.replace("dashboard.html");
        }else{
            $(this).parents('.modal-close').fadeOut('fast');
        }
    });

    $('.add-message').click(function(e){
        e.preventDefault();
        $('.modal-addContent').fadeIn('fast');
        $('.modal-addContent .form-image').hide();
        $('.modal-addContent .form-message').show();
        $('.modal-addContent .title-message').show();
        $('.modal-addContent .title-image').hide();

    });
    $('.add-picture').click(function(e){
        e.preventDefault();
        $('.modal-addContent').fadeIn('fast');
        $('.modal-addContent .form-image').show();
        $('.modal-addContent .form-message').hide();
        $('.modal-addContent .title-image').show();
        $('.modal-addContent .title-message').hide();

    });
    $('.modal-addContent .close-modal').click(function(e){
        e.preventDefault();
        $(this).parents('.modal-addContent').fadeOut('fast');
    })
    $(document).click(function(event) {
        //if you click on anything except the modal itself or the "open modal" link, close the modal
        if (!$(event.target).closest(".actions-container, .add-content-btn").length) {
          $("body").find('.actions-container').fadeOut('fast');
        }
      });
    $('.open-slider').click(function(){
        $('.modal-slider').fadeIn('fast');
    })
    $('.modal-slider .close-modal').click(function(e){
        e.preventDefault();
        $(this).parents('.modal-slider').fadeOut('fast');
        $('.slider-content').slick('unslick');
    })

    $('.sidebar .top-box .main-btn, .sidebar .bottom-box .main-btn').click(function(e){
        e.preventDefault();
        var contEle = $(this).parent().find('.show-el');
        $(contEle).find('.hidden').toggleClass('show-hidden');
        $(this).text($(this).text() == 'Ver más' ? 'Ver menos': 'Ver más')
    })

    $('.nav-icon').click(function(){
        $('.sidebar').toggleClass('show-side');
        $('.sidebar .top-box').show();
        $('.sidebar .bottom-box').hide();
        $('body').toggleClass('not-scroll');
    })
    $('.participants-icon').click(function(){
        $('.sidebar').toggleClass('show-side');
        $('.sidebar .top-box').hide();
        $('.sidebar .bottom-box').show();
        $('body').toggleClass('not-scroll');
    })

    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src)
        }
    };

    $('.form-image input[type="file"]').change(function(){
        loadFile(event);
    });

    if($('.slider-content').length){
        
        $('.slider-nav > div').click(function() {
            $('.slider-content').slick({
                adaptiveHeight: true
            });
            $('.slider-content').slick('slickGoTo',$(this).index());
        })
    }
    $('.new-message .close-message').click(function(e){
        e.preventDefault();
        $(this).parent().fadeOut('fast');
    })

    $('.modal-status-close').click(function(e){
        e.preventDefault();
        $(this).parent('.modal-status').fadeOut();
    })

    if($('header .top-nav').length){
        if(window.innerWidth < 768){
            var steps = [
                { 
                    intro: "Bienvenido a CÍRCULO GAVIRIA, conoce las funcionalidades de nuestra herramienta aquí."
                }, 
                {
                    element: document.querySelector('.step1m'),
                    intro: "En este menú podrás ver las cámaras al interior de la sala de conmemoración"
                },
                {
                    element: document.querySelector('.step2m'),
                    intro: "En este menú podrás tener una videollamada con el contratante del servicio"
                },
                {
                    element: document.querySelector('.step3'),
                    intro: "Aquí podrás cargar los mensajes de condolencia y las fotografías o videos de recuerdo que tengas con tu ser querido"
                },
                {
                    element: document.querySelector('.step4m'),
                    intro: "En esta opción podrás visualizar los mensajes de condolencia."
                },
                {
                    element: document.querySelector('.step5m'),
                    intro: "En esta opción podrás visualizar los recuerdos compartidos."
                }
            ];
        }else{
            var steps = [
                { 
                    intro: "Bienvenido a CÍRCULO GAVIRIA, conoce las funcionalidades de nuestra herramienta aquí."
                }, 
                {
                    element: document.querySelector('.step1'),
                    intro: "En este menú podrás ver las cámaras al interior de la sala de conmemoración"
                },
                {
                    element: document.querySelector('.step2'),
                    intro: "En este menú podrás tener una videollamada con el contratante del servicio"
                },
                {
                    element: document.querySelector('.step3'),
                    intro: "Aquí podrás cargar los mensajes de condolencia y las fotografías o videos de recuerdo que tengas con tu ser querido"
                },
                {
                    element: document.querySelector('.step4'),
                    intro: "En esta opción podrás visualizar los mensajes de condolencia."
                },
                {
                    element: document.querySelector('.step5'),
                    intro: "En esta opción podrás visualizar los recuerdos compartidos."
                }
            ];
        }
        function startIntro(){
            var intro = introJs();
            intro.setOptions({
                nextLabel: "continuar",
                showBullets: false,
                showStepNumbers: false,
                scrollToElement: false,
                doneLabel: "Listo para empezar",
                steps: steps
            });
    
            intro.start();
        }
        startIntro()
    }
})