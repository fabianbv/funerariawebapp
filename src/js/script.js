$(function(){
    $('.info-social a').click(function(e){
        e.preventDefault();
        var urlActual = window.location.href;
        var urlShare = encodeURIComponent(urlActual),
            textShareT = $('meta[property="twitter:description"]').attr("content") ? $('meta[property="twitter:description"]').attr("content") : 'Grupo Gaviria',
            dataSocial = $(this).attr("data-social");
        function open_Window(url, name) {
            window.open(url, name, 'height=320, width=640,toobar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no');
        } if (dataSocial === "facebook") {
            open_Window('https://www.facebook.com/sharer/sharer.php?u=' + urlShare, 'facebook_share');
        } else if (dataSocial === "twitter") {
            open_Window('https://twitter.com/intent/tweet?text=' + textShareT + " " + urlShare);
        }
    })

    $('.slider-media').slick({
        adaptiveHeight: true
    });

    $('.participants-item .main-btn').click(function(e){
        e.preventDefault();
        $('.modal-message').fadeIn();
    })
    $('.modal-message .close-modal').click(function(e){
        e.preventDefault();
        $(this).parents('.modal-message').fadeOut();
    })
});